import express, {Application} from 'express';
import cors from 'cors';
import routes from './routes';

const app: Application = express();
const serverPort: number = 5000;

// Middleware
app.use(cors());
app.use(express.json());

// Routes
app.use(routes.todo);

app.listen(serverPort, () => {
    console.log('Server has started on port ' + serverPort);
});
