import {Request, Response} from 'express';
import database from "../../database/";

export const getTodosByUserId = async (req: Request, res: Response): Promise<Response> => {
    // Get Values
    const id: string = req.params.id;

    // Database
    const response = await database.todo.GET_TODOS_BY_USER_ID(id);
    if ('error' in response) return res.status(500).json(response);

    return res.status(200).json(response);
};

export const getTodos = async (req: Request, res: Response): Promise<Response> => {
    // Database
    const response = await database.todo.GET_TODOS();
    if ('error' in response) return res.status(500).json(response);

    return res.status(200).json(response);
};

export const getTodoById = async (req: Request, res: Response): Promise<Response> => {
    // Get Values
    const id: string = req.params.id;

    // Database
    const response = await database.todo.GET_TODOS_BY_ID(id);
    if ('error' in response) return res.status(500).json(response);

    return res.status(200).json(response);
};

export const createTodo = async (req: Request, res: Response) => {
    // Get Values
    const {description, userId} = req.body;
    const creationDate = new Date().toUTCString();

    // Database
    const response = await database.todo.CREATE_TODO(userId, description, creationDate);
    if ('error' in response) return res.status(500).json(response);

    return res.status(200).json({
        success: {
            message: 'Successfully added Todo'
        },
        body: {
            todo: response
        }
    });
};

export const updateTodo = async (req: Request, res: Response) => {
    // Get Values
    const id = req.params.id;
    const {description} = req.body;

    // Database
    const response = await database.todo.UPDATE_TODO(id, description);
    if (response !== null && 'error' in response) return res.status(500).json(response);

    return res.status(200).json({
        success: {
            message: 'Successfully updated Todo'
        }
    });
};

export const deleteTodo = async (req: Request, res: Response) => {
    // Get Values
    const id = req.params.id;

    // Database
    const response = await database.todo.DELETE_TODO(id);
    if (response !== null && 'error' in response) return res.status(500).json(response);

    return res.status(200).json({
        success: {
            message: 'Successfully deleted Todo'
        }
    });
};

