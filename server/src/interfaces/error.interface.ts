type TErrorTypes = 'core' | 'input' | 'server';

export interface IError {
	error: {
		type: TErrorTypes
		message: string
		e: object | null
	}
}
