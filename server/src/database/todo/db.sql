CREATE DATABASE pulsetodo;

CREATE TABLE todo(
 pk SERIAL PRIMARY KEY,
 id UUID NOT NULL,
 userId VARCHAR(255),
 creationDate VARCHAR(255) ,
 description VARCHAR(255)
);
