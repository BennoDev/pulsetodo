import pool from '../../database/database';
import {QueryResult} from 'pg';
import {ITodo} from "../../interfaces/todo.interface";
import {IError} from "../../interfaces/error.interface";
import {v4 as uuidv4} from 'uuid';

export const GET_TODOS_BY_USER_ID = async (userId: string): Promise<ITodo[] | IError> => {
    try {
        const response: QueryResult = await
            pool.query('SELECT * FROM todo WHERE userId = $1', [userId]);
        return response.rows;
    } catch (e) {
        console.log(e);
        return {
            error: {
                message: 'Internal Server Error',
                type: 'server',
                e: e
            }
        }
    }
};

export const GET_TODOS = async (): Promise<ITodo[] | IError> => {
    try {
        const response: QueryResult = await
            pool.query('SELECT * FROM todo');
        return response.rows
    } catch (e) {
        console.log(e);
        return {
            error: {
                message: 'Internal Server Error',
                type: 'server',
                e: e
            }
        }
    }
};

export const GET_TODOS_BY_ID = async (id: string): Promise<ITodo | IError> => {
    try {
        const response: QueryResult = await pool.query('SELECT * FROM todo WHERE id = $1', [id]);
        return response.rows.length > 0 ? response.rows[0] : [];
    } catch (e) {
        console.log(e);
        return {
            error: {
                message: 'Internal Server Error',
                type: 'server',
                e: e
            }
        }
    }
};

export const CREATE_TODO = async (userId: string, description: string, creationDate: string, id?: string): Promise<ITodo | IError> => {
    let _id: string = id || uuidv4();
    try {
        // RETURNING todo.* returns the created row
        const response = await pool.query('INSERT INTO todo (id, description, userId, creationDate) VALUES($1, $2, $3, $4) RETURNING todo.*',
            [_id, description, userId, creationDate]);
        return {
            id: response.rows[0].id,
            userId: response.rows[0].userid,
            description: response.rows[0].description,
            creationDate: response.rows[0].creationdate
        }
    } catch (e) {
        console.log(e);
        return {
            error: {
                message: 'Internal Server Error',
                type: 'server',
                e: e
            }
        }
    }
};

export const UPDATE_TODO = async (id: string, description: string): Promise<null | IError> => {
    try {
        const response = await pool.query('UPDATE todo SET description = $1 WHERE id = $2', [
            description,
            id
        ]);
        return null;
    } catch (e) {
        console.log(e);
        return {
            error: {
                message: 'Internal Server Error',
                type: 'server',
                e: e
            }
        }
    }
};

export const DELETE_TODO = async (id: string): Promise<null | IError> => {
    try {
        const response = await pool.query('DELETE FROM todo where id = $1', [
            id
        ]);
        return null;
    } catch (e) {
        console.log(e);
        return {
            error: {
                message: 'Internal Server Error',
                type: 'server',
                e: e
            }
        }
    }
};
