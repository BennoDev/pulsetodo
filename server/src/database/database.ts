import {Pool} from 'pg';

const config = require("./config.user.json");

// Setup the PostGreSQL Database
const pool = new Pool({
    user: config.user,
    password: config.password,
    host: 'localhost',
    port: 5432,
    database: 'pulsetodo'
});

export default pool;
