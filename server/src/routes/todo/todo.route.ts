import { Router } from 'express';
import controllers from '../../controllers';

const router = Router();

router.get('/todos', controllers.todo.getTodos);
router.get('/todos/:id', controllers.todo.getTodoById);
router.get('/todos/user/:id', controllers.todo.getTodosByUserId);
router.post('/todos', controllers.todo.createTodo);
router.put('/todos/:id', controllers.todo.updateTodo);
router.delete('/todos/:id', controllers.todo.deleteTodo);

export default router;
