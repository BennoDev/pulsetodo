import core from '../../../core';

export const editTodo = (id: string, description: string) => {
	core.todos.actions.editTodo(id, description);
};

export const addTodo = (userId: string, description: string) => {
	core.todos.actions.addTodo(userId, description);
};

export const removeTodo = (id: string) => {
	core.todos.actions.removeTodo(id);
}


