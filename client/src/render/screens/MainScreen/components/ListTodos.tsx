import React, { Fragment, useCallback } from 'react';
import EditTodo from './EditTodo';
import { ITodo } from '../../../../core/controllers/todo/todo.interface';

interface Props {
	todos: ITodo[]
	removeTodo: (id: string) => void
	editTodo: (id: string, description: string) => void
}

const ListTodos: React.FC<Props> = (props) => {

	// Props
	const {todos, removeTodo, editTodo} = props;


	//=========================================================================================================
	// On Delete Todo
	//=========================================================================================================

	const onDeleteTodo = useCallback(async (id) => {
		removeTodo(id);
	}, [removeTodo]);


	//=========================================================================================================
	//Render
	//=========================================================================================================

	return (
		<Fragment>
			<table className="table mt-5 text-center">
				<thead>
				<tr>
					<th>Description</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
				</thead>
				<tbody>
				{todos.map(todo => (
					<tr key={todo.id}>
						{/* Description Colum */}
						<td>{todo.description}</td>

						{/* Edit Button Colum */}
						<td>
							<EditTodo
								todo={todo}
								editTodo={editTodo}
							/>
						</td>

						{/* Delete Button Colum */}
						<td>
							<button
								className="btn btn-danger"
								onClick={() => onDeleteTodo(todo.id)}
							>
								Delete
							</button>
						</td>
					</tr>
				))}
				</tbody>
			</table>
		</Fragment>
	);
};

export default ListTodos;
