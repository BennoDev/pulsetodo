import React, { Fragment, useState, useCallback } from 'react';

interface Props {
	addTodo: (description: string) => void
}

const InputTodo: React.FC<Props> = (props) => {

	// Props
	const {addTodo} = props;

	// Description
	const [description, setDescription] = useState('');


	//=========================================================================================================
	// On Submit From
	//=========================================================================================================

	const onSubmit = useCallback(async (event) => {
		//https://developer.mozilla.org/en-US/docs/Web/API/Event/preventDefault
		event.preventDefault();
		addTodo(description);
	}, [description, addTodo]);


	//=========================================================================================================
	// Render
	//=========================================================================================================

	return (
		<Fragment>
			<h1 className='text-center mt-5'>Todo List</h1>
			<form className="d-flex mt-5" onSubmit={onSubmit}>
				<input type="text" className="form-control" value={description}
							 onChange={input => setDescription(input.target.value)}/>
				<button className="btn btn-success">Add</button>
			</form>
		</Fragment>
	);
};

export default InputTodo;
