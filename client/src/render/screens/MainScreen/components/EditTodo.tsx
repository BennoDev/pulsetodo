import React, { Fragment, useState, useCallback } from 'react';
import { ITodo } from '../../../../core/controllers/todo/todo.interface';

interface Props {
	todo: ITodo
	editTodo: (id: string, description: string) => void
}

const EditTodo: React.FC<Props> = (props) => {

	// Props
	const { todo, editTodo } = props;

	// Description
	const [description, setDescription] = useState(todo.description);


	//=========================================================================================================
	// On Submit
	//=========================================================================================================

	const onSubmit = useCallback(async (event) => {
		event.preventDefault();
		editTodo(todo.id, description);
	}, [description, todo.id, editTodo]);


	//=========================================================================================================
	// Render Modal Selector
	//=========================================================================================================

	const renderModalSelector = () => {
		return (
			<div
				className="modal"
				id={`id${todo.id}`}
				onClick={() => setDescription(todo.description)}
			>
				<div className="modal-dialog">
					<div className="modal-content">
						<div className="modal-header">
							<h4 className="modal-title">Edit Todo</h4>
							<button
								type="button"
								className="close"
								data-dismiss="modal"
								onClick={() => setDescription(todo.description)}
							>
								&times;
							</button>
						</div>

						<div className="modal-body">
							<input
								type="text"
								className="form-control"
								value={description}
								onChange={e => setDescription(e.target.value)}
							/>
						</div>

						<div className="modal-footer">
							<button
								type="button"
								className="btn btn-warning"
								data-dismiss="modal"
								onClick={input => onSubmit(input)}
							>
								Edit
							</button>
							<button
								type="button"
								className="btn btn-danger"
								data-dismiss="modal"
								onClick={() => setDescription(todo.description)}
							>
								Close
							</button>
						</div>
					</div>
				</div>
			</div>
		);
	};


	//=========================================================================================================
	//Render
	//=========================================================================================================

	return (
		<Fragment>
			<button
				type="button"
				className="btn btn-warning"
				data-toggle="modal"
				data-target={`#id${todo.id}`}
			>
				Edit
			</button>
			{renderModalSelector()}
		</Fragment>
	);
};

export default EditTodo;
