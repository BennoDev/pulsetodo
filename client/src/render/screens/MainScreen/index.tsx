import React, {useCallback, useEffect, useState} from 'react';
import InputTodo from './components/InputTodo';
import ListTodos from './components/ListTodos';
import * as controller from './controller';
import {ITodo} from '../../../core/controllers/todo/todo.interface';
import {usePulse} from 'pulse-framework';
import core from '../../../core';
import {IUser} from '../../../core/controllers/user/user.interface';


const MainScreen = () => {

    // User
    const [user]: IUser[] = usePulse([core.user.state]);

    // Todos
    const [todos]: ITodo[][] = usePulse([core.todos.state.userTodos]);
    // console.log('Rerender');
    // console.log('Todos', todos);

    const [isLoading, setIsLoading] = useState<boolean>(false);


    //=========================================================================================================
    // Fetch Todo from User
    //=========================================================================================================

    useEffect(() => {
        const fetchTodos = async () => {
            setIsLoading(true);
            await core.todos.actions.fetchTodosByUserId(user.id);
            setIsLoading(false);
        }
        fetchTodos();
    }, [user.id]);


    //=========================================================================================================
    // Remove Todo
    //=========================================================================================================

    const removeTodo = useCallback(async (id) => {
        controller.removeTodo(id);
    }, []);


    //=========================================================================================================
    // Add Todo
    //=========================================================================================================

    const addTodo = useCallback(async (description: string) => {
        controller.addTodo(user.id, description);
    }, [user.id]);


    //=========================================================================================================
    // Edit Todo
    //=========================================================================================================

    const editTodo = useCallback(async (id: string, description: string) => {
        controller.editTodo(id, description);
    }, []);


    //=========================================================================================================
    // Render
    //=========================================================================================================

    return (
        <div className="container">
            <InputTodo
                addTodo={addTodo}
            />
            <ListTodos
                todos={todos}
                editTodo={editTodo}
                removeTodo={removeTodo}
            />
        </div>
    );
};

export default MainScreen;
