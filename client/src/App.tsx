import React, { Fragment } from 'react';
import MainScreen from './render/screens/MainScreen';

const App = () => {
	return (
		<Fragment>
			<MainScreen/>
		</Fragment>
	);
};

export default App;
