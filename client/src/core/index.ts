import {todos} from "./controllers/todo";
import {user} from "./controllers/user";

const core = {
    todos,
    user
};

export default core;
