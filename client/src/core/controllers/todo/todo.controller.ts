import {App} from '../../app';
import {ITodo} from './todo.interface';
import {Controller} from "pulse-framework";
import * as actions from './todo.actions';
import * as routes from './todo.routes';
import {user} from "../user";

const TodosCollection = App.Collection<ITodo>()(Collection => ({
    groups: {
        default: Collection.Group()
    },
    selectors: {}
}));

const userTodos = App.Computed(() => {
    console.log(user.state.value.id);
    console.log("TODO_LIST_USER ", TodosCollection.getGroup(user.state.value.id));
    console.log("TODO_LIST_DEFAULT ", TodosCollection.getGroup('default'));
    return TodosCollection.getGroup(user.state.value.id).output;
}, [TodosCollection.getGroup('default')]);

export const todos = new Controller({
    state: {userTodos: userTodos},
    collection: {
        todos: TodosCollection,
    },
    actions: actions,
    routes: routes,
    helpers: null
});
