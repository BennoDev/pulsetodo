import {ITodo} from "./todo.interface";
import {IError} from "../../interfaces/error.interface";
import api from "../../api";

interface IAddTodoPayload {
    description: string,
    userId: string
}

type TAddTodoResponse = ITodo | IError;

export const ADD_TODO = async (payload: IAddTodoPayload): Promise<TAddTodoResponse> => {
    const response = await api.post('todos', payload);
    if (response.status !== 200) {
        if ('error' in response.data) return response.data.error;
        return {
            error: {
                message: 'Unknown Error ' + response.status,
                type: 'server',
                e: null
            }
        };
    }

    return response.data.body.todo;
}

type TRemoveTodoResponse = null | IError;

export const REMOVE_TODO = async (id: string): Promise<TRemoveTodoResponse> => {
    const response = await api.delete('todos/' + id);
    if (response.status !== 200) {
        if ('error' in response.data) return response.data.error;
        return {
            error: {
                message: 'Unknown Error ' + response.status,
                type: 'server',
                e: null
            }
        };
    }

    return null;
}

interface IEditTodoPayload {
    description: string
}

type TEditTodoResponse = null | IError;

export const EDIT_TODO = async (id: string, payload: IEditTodoPayload): Promise<TEditTodoResponse> => {
    const response = await api.put('todos/' + id, payload);
    if (response.status !== 200) {
        if ('error' in response.data) return response.data.error;
        return {
            error: {
                message: 'Unknown Error ' + response.status,
                type: 'server',
                e: null
            }
        };
    }

    return null;
}

type TFetchTodoByUserIdResponse = IError | ITodo[];

export const FETCH_TODO_BY_USER_ID = async (userId: string): Promise<TFetchTodoByUserIdResponse> => {
    // Api
    const response = await api.get('todos/user/' + userId);
    if (response.status !== 200) {
        if ('error' in response.data) return response.data.error;
        return {
            error: {
                message: 'Unknown Error ' + response.status,
                type: 'server',
                e: null
            }
        };
    }

    return response.data;
}
