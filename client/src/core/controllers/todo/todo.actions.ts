import {ITodo} from './todo.interface';
import {IError} from '../../interfaces/error.interface';
import core from "../../index";

export const addTodo = async (userId: string, description: string): Promise<IError | null> => {
    console.log('Add TODO');

    // Api
    const response = await core.todos.routes.ADD_TODO({description: description, userId: userId});
    if ('error' in response) return response;
    const todo = response;

    // Core
    core.todos.collection.todos.collect(todo, userId);
    core.todos.collection.todos.collect(todo, 'default');

    return null;
};

export const removeTodo = async (id: string): Promise<IError | null> => {
    console.log('Remove TODO');

    // Api
    const response = await core.todos.routes.REMOVE_TODO(id);
    if (response !== null && 'error' in response) return response;

    // Core
    core.todos.collection.todos.remove(id).everywhere();

    return null;
};

export const editTodo = async (id: string, description: string): Promise<IError | null> => {
    console.log('Edit TODO');

    // Api
    const response = await core.todos.routes.EDIT_TODO(id, {description: description});
    if (response !== null && 'error' in response) return response;

    // Update Todo
    const todo = core.todos.collection.todos.findById(id).value;
    todo.description = description;

    // Core
    core.todos.collection.todos.update(id, todo);

    return null;
};

export const fetchTodosByUserId = async (userId: string): Promise<IError | ITodo[]> => {
    console.log('Fetch TODO by UserId ' + userId);

    // Api
    const response = await core.todos.routes.FETCH_TODO_BY_USER_ID(userId);
    if ('error' in response) return response;
    const _todos: ITodo[] = response;

    // Core
    core.todos.collection.todos.collect(_todos, userId);
    core.todos.collection.todos.collect(_todos, 'default');

    return _todos;
};
