import {App} from '../../app';
import {IUser} from './user.interface';
import {Controller} from "pulse-framework";
import * as actions from './user.actions';

const USER = App.State<IUser>({
    id: 'c3541393-7d6f-4301-9014-2bdae0262f47',
    name: 'Jeff'
}).persist('user');

export const user = new Controller({
    state: USER,
    collection: null,
    actions: actions,
    routes: null,
    helpers: null
});
