import { IUser } from './user.interface';
import core from "../../index";

export const setUser = (user: IUser) => {
	core.user.state.set(user);
};
