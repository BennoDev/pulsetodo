Note: The React Code (MainScreen) I stole from https://www.youtube.com/watch?v=ldYcgPKEZC8 ^^

# Get Started

For this dummy project you need [**Postgresql**](https://www.google.com/search?client=firefox-b-d&q=postgresql), [**NodeJs**](https://nodejs.org/en/) and [**React**](https://reactjs.org/)

## Creating Postgresql Database
1) Open Terminal and login with your user `psql -U postgres` (Note: postgres is the default user of Postgresql which will be created during the installation process)
2) Create Database with the first part of SQL 'code' (See: **server/src/database/db.sql**)
3) Go into this database `\c pulsetodo`
4) Create Table with the second part of 'code' (See: **server/src/database/db.sql**) 
5) Check if table has been created with `\dt`
6) Success

## Start server
1) Open the Terminal and navigate to the project 
2) Navigate to server `cd server`
3) If you have pulled the project for the first time make sure to run `npm install`
4) If you haven't done yet.. create a `config.user.json` file in **src/database** which should include your Postgresql **user name** and **password**
   ```
   {
     "user": "postgres",
     "password": "YOUR_PASSWORD"
   }
   ```
4) Run `npm run-script dev` or `nodemon src/index.ts`
5) Success

## Start client
1) Open the Terminal and navigate to the project 
2) Navigate to client `cd client`
3) If you have pulled the project for the first time make sure to run `npm install`
4) Run `npm start`
5) Success
